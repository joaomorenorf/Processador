library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity Controle is

	generic (
		tDados		: natural := 16;
		tMnemonico	: natural :=  4;
		nbitsCodOp	: natural :=  3
	);

	port (
		relogio		: in  std_logic;
		reset		: in  std_logic;
		-- Nao ha Controle
		-- Entrada
		instrucao	: in  std_logic_vector(tDados		- 1 downto 0);
		--Saída
		-- Unidade Logica Aritimetica
		operacao	: out std_logic_vector(nbitsCodOp	- 1 downto 0);
		ativaAri   	: out std_logic;
		ativaComp	: out std_logic;
		ativaLogi	: out std_logic;
		-- Banco de Registradores
		ativaEBR	: out std_logic;									-- Ativa entrada de dados
		ativaA		: out std_logic;									-- Ativa entrada de valor alto
		-- Memoria
		ativaMemE	: out std_logic;
		-- B ou I
		BouI 	    : out std_logic;									-- 0 = Banco e 1 = Imediato
		-- U ou M
		UouM 	    : out std_logic										-- 0 = ULA e 1 = Memoria
	);

end entity;

architecture rtl of Controle is

signal desativa: std_logic;

begin

process(instrucao, reset, relogio, desativa) is
begin
	if rising_edge(relogio) then
		if reset = '1' then
			desativa <= '1';
		else
			desativa <= '0';
		end if;
	end if;
	
	-- Unidade Logica Aritimetica
	operacao  <= (others => '0');
	ativaAri  <= '0';
	ativaComp <= '0';
	ativaLogi <= '0';
	-- Banco de Registradores
	ativaEBR  <= '0';				-- Ativa entrada de dados
	ativaA	  <= '0';				-- Ativa entrada de valor alto
	-- Memoria
	ativaMemE <= '0';
	-- B ou I
	BouI 	  <= '0';
	-- U ou M
	UouM 	  <= '0';
	
	if desativa = '0' then
		case instrucao(tDados - 1 downto tDados - tMnemonico) is
			when "0000" =>
				-- Aritimetica
				ativaAri  <= '1';
				operacao  <= instrucao(2  downto 0);
				ativaEBR  <= '1';
			when "0001" =>
				-- Adiciona Imediato
				ativaAri  <= '1';
				BouI 	  <= '1';
				ativaEBR  <= '1';
	--		when "0010" =>
				-- Pula
			when "0011" =>
				-- Se
				ativaComp <= '1';
				operacao  <= instrucao(2  downto 0);
				ativaEBR  <= '1';
			when "0100" =>
				-- Pega Imediato
				ativaAri  <= '1';
				BouI 	  <= '1';
				ativaEBR  <= '1';
			when "0101" =>
				-- Pega Memoria
				UouM 	  <= '1';
				ativaEBR  <= '1';
			when "0110" =>
				-- Guarda
				ativaMemE <= '1';
			when "0111" =>
				-- Logicas
				ativaLogi <= '1';
				operacao  <= instrucao(2  downto 0);
				ativaEBR  <= '1';
	--		when "1000" =>
				--
	--		when "1001" =>
				--
	--		when "1010" =>
				--
	--		when "1011" =>
				--
	--		when "1100" =>
				--
	--		when "1101" =>
				--
	--		when "1110" =>
				--
	--		when "1111" =>
				--
			when others =>
		end case;
	end if;
end process;
end rtl;
