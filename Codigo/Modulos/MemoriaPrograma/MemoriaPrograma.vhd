library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity MemoriaPrograma is

generic(
	tDados			: natural := 16;
	tMemoriaP		: natural := 12 --2^12 = 4096
);

port(
	relogio  		: in  std_logic;
	-- Controle
	ativaE			: in  std_logic;
	-- Entrada
	valorE			: in  std_logic_vector(tDados		- 1 downto 0);
	enderecoE		: in  std_logic_vector(tMemoriaP	- 1 downto 0);
	enderecoL		: in  std_logic_vector(tMemoriaP	- 1 downto 0);
	-- Saida
	valorS			: out std_logic_vector(tDados		- 1 downto 0)
);

end MemoriaPrograma;

architecture MemoriaPrograma of MemoriaPrograma is

type matriz is array(2**tMemoriaP - 1 downto 0) of std_logic_vector(tDados	- 1 downto 0);
signal Memoria : matriz;

begin
process(relogio, enderecoL, ativaE, enderecoE, Memoria, valorE)
begin
	valorS <= Memoria(to_integer(unsigned(enderecoL)));
	if (falling_edge(relogio)) then
		if ativaE = '1' then
			Memoria(to_integer(unsigned(enderecoE))) <= valorE;
		end if;
	end if;
end process;
end MemoriaPrograma;
