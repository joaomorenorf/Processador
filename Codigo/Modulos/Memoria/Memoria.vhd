library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity Memoria is

	generic(
		tDados		: natural := 16;
		tMemoria	: natural :=  9 --2^9 = 512
	);

	port(
		relogio  	: in  std_logic;
		-- Controle
		ativaE		: in  std_logic;
		-- Entrada
		valorE		: in  std_logic_vector(tDados	- 1 downto 0);
		endereco	: in  std_logic_vector(tMemoria - 1 downto 0);
		-- Saida
		valorS		: out std_logic_vector(tDados	- 1 downto 0);
		-- Entradas e saidas especiais
		entradaE	: in  std_logic_vector(tDados	- 1 downto 0);
		saidaE		: out std_logic_vector(tDados	- 1 downto 0)
	);

end Memoria;

architecture Memoria of Memoria is

type matriz is array(2**tMemoria - 1 downto 0) of std_logic_vector(tDados	- 1 downto 0);
signal Memoria : matriz;

begin
process(relogio, endereco, ativaE, valorE, Memoria)
begin
	saidaE	<= Memoria(0);
	valorS	<= Memoria(to_integer(unsigned(endereco)));
	if (falling_edge(relogio)) then
		Memoria(1)	<= entradaE;
		if ativaE = '1' then
			Memoria(to_integer(unsigned(endereco))) <= valorE;
		end if;
	end if;
end process;
end Memoria;
