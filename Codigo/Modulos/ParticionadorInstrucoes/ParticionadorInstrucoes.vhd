library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ParticionadorInstrucoes is

	generic (
		tDados		: natural := 16;
		tMemoriaBR	: natural :=  3;	--2^3  = 8
		tMemoria	: natural :=  9; 	--2^9  = 512
		tImediato	: natural :=  9;
		tMnemonico	: natural :=  4
	);

	port (
		-- Entrada
		instrucao	: in  std_logic_vector(tDados		- 1 downto 0);
		-- Sem Controle
		--Saida
		enderecoMem	: out std_logic_vector(tMemoria	- 1 downto 0);
		imediato 	: out std_logic_vector(tDados		- 1 downto 0);
		REG1		: out std_logic_vector(tMemoriaBR	- 1 downto 0);
		REG2		: out std_logic_vector(tMemoriaBR	- 1 downto 0);
		REG3		: out std_logic_vector(tMemoriaBR	- 1 downto 0)
	);

end entity;

architecture rtl of ParticionadorInstrucoes is
begin
process(instrucao)
begin
	enderecoMem <= instrucao(tMemoria - 1 downto 0);
	imediato <= std_logic_vector(resize(unsigned(instrucao(tImediato - 1 downto 0)), tDados));-- Mnemonico (tDados - 1 downto 12)
	REG1 <= instrucao(tDados - tMnemonico - 0*tMemoriaBR - 1 downto tDados - tMnemonico - 1*tMemoriaBR); 	 	-- (11 downto  9)
	case instrucao(tDados - 1 downto tDados - tMnemonico) is
		when "0001" => -- Adiciona Imediato
			REG2 <= instrucao(tDados - tMnemonico - 0*tMemoriaBR - 1 downto tDados - tMnemonico - 1*tMemoriaBR);-- (11 downto  9)
		when "0100" => -- Pega Imediato
		REG2 <= (others => '0'); 																				-- (zero)
		when others =>
			REG2 <= instrucao(tDados - tMnemonico - 1*tMemoriaBR - 1 downto tDados - tMnemonico - 2*tMemoriaBR);-- ( 8 downto  6)
	end case;
	REG3 <= instrucao(tDados - tMnemonico - 2*tMemoriaBR - 1 downto tDados - tMnemonico - 3*tMemoriaBR); 	 	-- ( 5 downto  3)
end process;
end rtl;
