library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity BouI is

	generic (
		tDados	: natural := 16
	);

	port (
		-- Controle
		BouI	: in  std_logic;
		-- Entrada
		Banco	: in  std_logic_vector(tDados	- 1 downto 0);
		Imedi	: in  std_logic_vector(tDados	- 1 downto 0);
		-- Saida
		Saida 	: out std_logic_vector(tDados	- 1 downto 0)
	);

end entity;

architecture rtl of BouI is
begin
process(BouI, Banco, Imedi) is
begin
	if (BouI = '0') then
		Saida <= Banco;
	else
		Saida <= Imedi;
	end if;
end process;
end rtl;
