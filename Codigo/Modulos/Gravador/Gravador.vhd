library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity Gravador is

	generic (
		tDados		: natural := 16;
		tMemoriaP	: natural := 12;
		instAGravar	: natural :=  6
	);

	port (
		relogio		: in  std_logic;
		-- Entrada para evitar simplificacao do design
		inst		: in  std_logic_vector(tDados		- 1 downto 0);
		ende		: in  std_logic_vector(tMemoriaP	- 1 downto 0);
		-- Controle externo
		ativaGrav	: in  std_logic;
		ativaGravD	: in  std_logic;
		--Saida
		gravando	: out std_logic;
		enderecoMP	: out std_logic_vector(tMemoriaP	- 1 downto 0);
		instrucao	: out std_logic_vector(tDados		- 1 downto 0)
	);

end entity;

architecture rtl of Gravador is

type matrizE is array(0 to instAGravar - 1) of std_logic_vector(12	- 1 downto 0);
constant ListaE : matrizE :=
("000000000001",--PEGA IMEDIATO
 "000000000010",--GUARDA
 "000000000011",--PEGA
 "000000000100",--GUARDA
 "000000000101",--SOMA 1
 "000000000110" --PULA 0b10
);
type matrizI is array(0 to instAGravar - 1) of std_logic_vector(tDados		- 1 downto 0);
constant ListaI : matrizI :=
("0100001010101010",--PEGA IMEDIATO 42AA
 "0110001000000001",--GUARDA		6201
 "0101010000000000",--PEGA			5400
 "0110010000000001",--GUARDA		6401
 "0000001000001100",--SOMA 1		020C
 "0010000000000010" --PULA 0b10		2002
);

signal controle : natural := 0;

begin
process(relogio, ativaGrav)
begin
	if rising_edge(relogio) then
		if ativaGrav = '1' then
			gravando	<= '1';
			if controle < instAGravar then
				enderecoMP	<= ListaE(controle)(tMemoriaP - 1 downto 0);
				instrucao	<= ListaI(controle);
				controle <= controle + 1;
			else
				controle <= 0;
			end if;
		elsif ativaGravD = '1' then
			gravando	<= '1';
			enderecoMP	<= ende;
			instrucao	<= inst;
		else
			enderecoMP	<= (others => '0');
			instrucao	<= (others => '0');
			gravando	<= '0';
			controle <= 0;
		end if;
	end if;
end process;
end rtl;
