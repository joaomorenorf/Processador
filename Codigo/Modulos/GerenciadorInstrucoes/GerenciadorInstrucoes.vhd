library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity GerenciadorInstrucoes is

	generic (
		tDados		: natural := 16;
		tMnemonico	: natural :=  4;
		tMemoria	: natural := 12 --2^12 = 4096
	);

	port (
		relogio		: in  std_logic;
		reset		: in  std_logic;
		-- Controle
		gravando	: in  std_logic;
		sucessoComp	: in  std_logic;
		-- Entrada
		instrucao 	: in  std_logic_vector(tDados 	 - 1 downto 0);
		-- Saida
		endProximo	: out std_logic_vector(tMemoria	 - 1 downto 0)
	);

end GerenciadorInstrucoes;

architecture GerenciadorInstrucoes of GerenciadorInstrucoes is

signal endAtual: std_logic_vector (tMemoria - 1 downto 0);

begin

process(relogio, endAtual)
begin
	if rising_edge(relogio) then
		if 	reset  = '0' AND gravando = '0' then
			endProximo <= endAtual;
		else
			endProximo <= (others => '0'); 
		end if;
	end if;
end process;

process(relogio, reset, gravando, sucessoComp, instrucao)
begin
	if 	falling_edge(relogio) then
		if 	reset  = '0' AND gravando = '0' then
			case instrucao(tDados - 1 downto tDados - tMnemonico) is
				when "0010" => --Pulo
					endAtual <= instrucao(tMemoria - 1 downto 0);
				when "0011" => -- Comparacao
					if sucessoComp = '1' then 								-- Soma 2 caso a comparacao estiver correta
						endAtual <= std_logic_vector(unsigned(endAtual) + 2);
					else
						endAtual <= std_logic_vector(unsigned(endAtual) + 1);
					end if;
				when others =>
					endAtual <= std_logic_vector(unsigned(endAtual) + 1);	-- Soma 1 como o normal
			end case;
		else																-- Se estiver durante uma gravacao ou reset
			endAtual <= (others => '0'); 									-- Volta para a primeira instrucao
		end if;
	end if;
end process;
end GerenciadorInstrucoes;
