library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity UouM is

	generic (
		tDados	: natural := 16
	);

	port (
		-- Controle
		UouM	: in  std_logic;
		-- Entrada
		ULA		: in  std_logic_vector(tDados	- 1 downto 0);
		Mem		: in  std_logic_vector(tDados	- 1 downto 0);
		-- Saida
		Saida	: out std_logic_vector(tDados	- 1 downto 0)
	);

end entity;

architecture rtl of UouM is
begin
process(UouM, ULA, Mem) is
begin
	if (UouM = '0') then
		Saida <= ULA;
	else
		Saida <= Mem;
	end if;
end process;
end rtl;
