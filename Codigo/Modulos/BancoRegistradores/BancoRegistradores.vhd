library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity BancoRegistradores is

	generic(
		tDados		: natural := 16;
		tMemoriaBR	: natural := 3; --2^3 = 8
		endZero 	: natural := 0
	);

	port(
		relogio  	: in  std_logic;									-- Relogio
		-- Controle
		ativaE		: in  std_logic;									-- Ativa entrada de dados
		ativaA		: in  std_logic;									-- Ativa entrada de valor alto
		-- Entradas
		valorE		: in  std_logic_vector(tDados		- 1 downto 0);	-- Valor a ser escrevido
		valorA		: in  std_logic_vector(tDados		- 1 downto 0);	-- Valor alto a ser gravado
		enderecoL0	: in  std_logic_vector(tMemoriaBR	- 1 downto 0);	-- Endereco utilizado
		enderecoL1	: in  std_logic_vector(tMemoriaBR	- 1 downto 0);	-- Endereco utilizado pela segunda leitura
		enderecoE	: in  std_logic_vector(tMemoriaBR	- 1 downto 0);	-- Endereco utilizado pela escrita
		-- Saidas
		valorS0		: out std_logic_vector(tDados		- 1 downto 0);	-- Valor de saida
		valorS1		: out std_logic_vector(tDados		- 1 downto 0)	-- Valor de saida da segunda leitura
	);

end BancoRegistradores;

architecture BancoRegistradores of BancoRegistradores is

type matriz is array(2**tMemoriaBR - 1 downto 0) of std_logic_vector(tDados	- 1 downto 0);
signal Memoria : matriz;

begin

process(relogio, enderecoL0, enderecoL1, ativaE, ativaA, enderecoE, Memoria)
begin
	valorS0 <= Memoria(to_integer(unsigned(enderecoL0)));
	valorS1 <= Memoria(to_integer(unsigned(enderecoL1)));
	if (falling_edge(relogio)) then
		if ativaA = '1' then
			Memoria(7) <= valorA;
		end if;
		if ativaE = '1' then
			Memoria(to_integer(unsigned(enderecoE))) <= valorE;
		end if;
		Memoria(endZero) <= (others => '0');
	end if;
end process;
end BancoRegistradores;
