library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_signed.all;

entity Aritmetica is

	generic (
		tDados		: natural := 16;
		nbitsCodOp	: natural := 3
	);

	port (
		entrada0	: in  std_logic_vector(tDados		- 1 downto 0);
		entrada1	: in  std_logic_vector(tDados		- 1 downto 0);
		operacao  	: in  std_logic_vector(nbitsCodOp 	- 1 downto 0);
		resultado 	: out std_logic_vector(tDados		- 1 downto 0);
		saidaAlta 	: out std_logic_vector(tDados		- 1 downto 0)
	);

end Aritmetica;

architecture Aritmetica of Aritmetica is

signal variavel : std_logic_vector (2*tDados - 1 downto 0);

begin
process(entrada0, entrada1, operacao,variavel)
begin
	case operacao is
		when "000" => -- +
			variavel <= std_logic_vector(resize(unsigned(entrada0), 2*tDados)  +  resize(unsigned(entrada1), 2*tDados));
		when "001" => -- -
			variavel <= std_logic_vector(resize(unsigned(entrada0), 2*tDados)  -  resize(unsigned(entrada1), 2*tDados));
		when "010" => -- *
			variavel <= std_logic_vector(		unsigned(entrada0)			   * 		 unsigned(entrada1));
		when "011" => -- /
			variavel <= std_logic_vector(resize(unsigned(entrada0), 2*tDados)  /  resize(unsigned(entrada1), 2*tDados));
		when "100" => -- ++
			variavel <= std_logic_vector(resize(unsigned(entrada1), 2*tDados)  + 1);
		when "101" => -- --
			variavel <= std_logic_vector(resize(unsigned(entrada1), 2*tDados)  - 1);
		when "110" => -- enche
			variavel <= (others => '1');
		when "111" => -- mod
			variavel <= std_logic_vector(resize(unsigned(entrada0), 2*tDados) mod resize(unsigned(entrada1), 2*tDados));
		when others => variavel <= (others => '0');
	end case;
	resultado <= variavel((tDados	  - 1) downto 0     );
	saidaAlta <= variavel((tDados*2 - 1) downto tDados);
end process;
end Aritmetica;