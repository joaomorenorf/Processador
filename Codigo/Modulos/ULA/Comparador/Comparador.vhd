library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_signed.all;

entity Comparador is

	generic
	(
		tDados: 		 natural := 16;
		nbitsCodOp:	 natural := 3
	);

	port 
	(
		entrada0 : in  std_logic_vector ((tDados	   - 1) downto 0);
		entrada1 : in  std_logic_vector ((tDados		- 1) downto 0);
		operacao : in  std_logic_vector ((nbitsCodOp - 1) downto 0);
		saida    : out std_logic
	);

end Comparador;

architecture Comparador of Comparador is
begin
process(entrada0, entrada1, operacao)
begin
	case operacao is
		when "000" => -- =
			if entrada0 = entrada1 then
				saida <= '1';
			else 
				saida <= '0';
			end if;
		when "001" => -- >
			if entrada0 < entrada1 then
				saida <= '1';
			else 
				saida <= '0';
			end if;
		when "010" => -- >=
			if entrada0 <= entrada1 then
				saida <= '1';
			else 
				saida <= '0';
			end if;
		when "100" => -- /=
			if entrada0 /= entrada1 then
				saida <= '1';
			else 
				saida <= '0';
			end if;
		when "101" => -- <
			if entrada0 > entrada1 then
				saida <= '1';
			else 
				saida <= '0';
			end if;
		when "110" => -- <=
			if entrada0 >= entrada1 then
				saida <= '1';
			else 
				saida <= '0';
			end if;
		when others => saida <= '0';
	end case;
end process;
end Comparador;