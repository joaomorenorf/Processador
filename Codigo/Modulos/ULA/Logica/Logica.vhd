library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity Logica is

	generic
	(
		tDados		: natural := 16;
		nbitsCodOp	: natural := 3
	);

	port 
	(
		entrada0	: in  std_logic_vector(tDados	  - 1 downto 0);
		entrada1	: in  std_logic_vector(tDados	  - 1 downto 0);
		operacao  	: in  std_logic_vector(nbitsCodOp - 1 downto 0);
		resultado 	: out std_logic_vector(tDados	  - 1 downto 0)
	);

end Logica;

architecture Logica of Logica is
begin
process(entrada0, entrada1, operacao)
begin
	case operacao is
		when "000" => 
			resultado <= entrada0 AND  entrada1;
		when "001" =>
			resultado <= entrada0 OR   entrada1;
		when "010" =>
			resultado <= entrada0 XOR  entrada1;
		when "011" =>
			resultado <= 		  NOT  entrada1;
		when "100" =>
			resultado <= entrada0 NAND entrada1;
		when "101" =>
			resultado <= entrada0 NOR  entrada1;
		when "110" =>
			resultado <= entrada0 XNOR entrada1;
		when "111" =>
			resultado <= (others => '0');
	end case;
end process;
end Logica;