library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ULA is

	generic (
		tDados:		natural := 16;
		nbitsCodOp:	natural := 3
	);

	port (
		-- Controle
		ativaAri	: in  std_logic;
		ativaComp	: in  std_logic;
		ativaLogi	: in  std_logic;
		operacao	: in  std_logic_vector(nbitsCodOp	- 1 downto 0);
		-- Entradas
		entrada0 	: in  std_logic_vector(tDados		- 1 downto 0);
		entrada1 	: in  std_logic_vector(tDados		- 1 downto 0);
		-- Saidas
		valorSai	: out std_logic_vector(tDados		- 1 downto 0);
		valorAlt	: out std_logic_vector(tDados		- 1 downto 0);
		modifCP		: out std_logic
	);

end ULA;

architecture ULA of ULA is

component Comparador is

	generic (
		tDados: 	natural;
		nbitsCodOp:	natural
	);

	port (
		entrada0 : in  std_logic_vector(tDados		- 1 downto 0);
		entrada1 : in  std_logic_vector(tDados		- 1 downto 0);
		operacao : in  std_logic_vector(nbitsCodOp	- 1 downto 0);
		saida    : out std_logic
	);

end component;

component Aritmetica is

	generic (
		tDados:		natural;
		nbitsCodOp:	natural
	);

	port (
		entrada0	: in  std_logic_vector(tDados		- 1 downto 0);
		entrada1	: in  std_logic_vector(tDados		- 1 downto 0);
		operacao	: in  std_logic_vector(nbitsCodOp	- 1 downto 0);
		resultado	: out std_logic_vector(tDados		- 1 downto 0);
		saidaAlta	: out std_logic_vector(tDados		- 1 downto 0)
	);

end component;

component Logica is

	generic (
		tDados: 	natural;
		nbitsCodOp:	natural
	);

	port (
		entrada0	: in  std_logic_vector(tDados		- 1 downto 0);
		entrada1	: in  std_logic_vector(tDados		- 1 downto 0);
		operacao	: in  std_logic_vector(nbitsCodOp	- 1 downto 0);
		resultado	: out std_logic_vector(tDados		- 1 downto 0)
	);

end component;

signal saidaComp : std_logic;
signal saidaAri, saidaLogi, saidaAlta: std_logic_vector(tDados - 1 downto 0);

begin
C1: Comparador	generic map (tDados, nbitsCodOp) port map (entrada0, entrada1, operacao, saidaComp);
-- entrada0 entrada1 operacao saida
C2: Aritmetica	generic map (tDados, nbitsCodOp) port map (entrada0, entrada1, operacao, saidaAri, saidaAlta);
-- entrada0 entrada1 operacao resultado alto
C3: Logica		generic map (tDados, nbitsCodOp) port map (entrada0, entrada1, operacao, saidaLogi);
-- entrada0 entrada1 operacao resultado

process(ativaAri, ativaComp, ativaLogi, saidaComp, saidaAri, saidaAlta, saidaLogi)
begin
	if 	ativaAri  = '1' then -- Aritmetica
		valorSai <= saidaAri;
		valorAlt <= saidaAlta;
		modifCP	 <= '0';

	elsif ativaComp = '1' then -- Comparador
		valorSai(tDados - 1 downto 1) <= (others => '0');
		valorSai(0) <= saidaComp;
		valorAlt	<= (others => '0');
		modifCP		<= saidaComp;

	elsif ativaLogi = '1' then -- Logica
		valorSai	<= saidaLogi;
		valorAlt	<= (others => '0');
		modifCP		<= '0';

	else								-- Desativado
		valorSai	<= (others => '0');
		valorAlt	<= (others => '0');
		modifCP		<= '0';
	end if;
end process;
end ULA;
