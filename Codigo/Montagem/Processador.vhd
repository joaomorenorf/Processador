library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity Processador is

	generic (
		tDados		: natural := 16;
		tMemoriaBR	: natural :=  3; --2^3  = 8
		tMemoria	: natural :=  9; --2^12 = 4096
		tMemoriaP	: natural := 9; --2^12 = 4096
		tImediato	: natural :=  9;
		nbitsCodOp	: natural :=  3;
		tMnemonico	: natural :=  4;
		endZeroBR	: natural :=  0
	);

	port (
		-- Testes
		-- Gravador
		--grav 		: out std_logic;
		--enderecoEMPG: out std_logic_vector(tMemoriaP	- 1 downto 0);
		--instrucaoPG	: out std_logic_vector(tDados		- 1 downto 0);
		-- Memoria de Programa
		--endLMP		: out std_logic_vector(tMemoriaP	- 1 downto 0);
		instru		: out std_logic_vector(tDados 		- 1 downto 0);
		-- Particionador de Instrucoes
		endM		: out std_logic_vector(tMemoria		- 1 downto 0);
		PpB			: out std_logic_vector(tDados		- 1 downto 0);
		endEBR		: out std_logic_vector(tMemoriaBR	- 1 downto 0);
		endL0BR		: out std_logic_vector(tMemoriaBR	- 1 downto 0);
		endL1BR		: out std_logic_vector(tMemoriaBR	- 1 downto 0);
		
		-- Testes
		
		relogio		: in  std_logic;
		reset		: in  std_logic;
		-- Entradas
		ativaGrav	: in  std_logic;
		ativaGravD	: in  std_logic;
		entradaE	: in  std_logic_vector(tDados		- 1 downto 0);
		inst		: in  std_logic_vector(tDados		- 1 downto 0);
		ende		: in  std_logic_vector(tMemoriaP	- 1 downto 0);
		-- Saidas
		saidaE		: out std_logic_vector(tDados		- 1 downto 0)
	);

end Processador;

architecture rtl of Processador is

component  BancoRegistradores is

	generic(
		tDados		: natural := 16;
		tMemoriaBR	: natural := 3; --2^3 = 8
		endZero 	: natural := 0
	);

	port(
		relogio  	: in  std_logic;								-- Relogio
		-- Controle
		ativaE		: in  std_logic;								-- Ativa entrada de dados
		ativaA		: in  std_logic;								-- Ativa entrada de valor alto
		-- Entradas
		valorE		: in  std_logic_vector(tDados	  - 1 downto 0);-- Valor a ser escrevido
		valorA		: in  std_logic_vector(tDados	  - 1 downto 0);-- Valor alto a ser gravado
		enderecoL0	: in  std_logic_vector(tMemoriaBR - 1 downto 0);-- Endereco utilizado
		enderecoL1	: in  std_logic_vector(tMemoriaBR - 1 downto 0);-- Endereco utilizado pela segunda leitura
		enderecoE	: in  std_logic_vector(tMemoriaBR - 1 downto 0);-- Endereco utilizado pela escrita
		-- Saidas
		valorS0		: out std_logic_vector(tDados	  - 1 downto 0);-- Valor de saida
		valorS1		: out std_logic_vector(tDados	  - 1 downto 0)	-- Valor de saida da segunda leitura
	);

end component;

component BouI is

	generic (
		tDados		: natural := 16
	);

	port (
		-- Controle
		BouI		: in  std_logic;
		-- Entrada
		Banco		: in  std_logic_vector(tDados	- 1 downto 0);
		Imedi		: in  std_logic_vector(tDados	- 1 downto 0);
		-- Saida
		Saida 		: out std_logic_vector(tDados	- 1 downto 0)
	);

end component;

component Controle is

	generic (
		tDados		: natural := 16;
		tMnemonico	: natural :=  4;
		nbitsCodOp	: natural :=  3
	);

	port (
		reset		: in  std_logic;
		relogio		: in  std_logic;
		-- Nao ha Controle
		-- Entrada
		instrucao	: in  std_logic_vector(tDados		- 1 downto 0);
		--Saída
		-- Unidade Logica Aritmetica
		operacao	: out std_logic_vector(nbitsCodOp	- 1 downto 0);
		ativaAri   	: out std_logic;
		ativaComp	: out std_logic;
		ativaLogi	: out std_logic;
		-- Banco de Registradores
		ativaEBR	: out std_logic;									-- Ativa entrada de dados
		ativaA		: out std_logic;									-- Ativa entrada de valor alto
		-- Memoria
		ativaMemE	: out std_logic;
		-- B ou I
		BouI 	    : out std_logic;									-- 0 = Banco e 1 = Imediato
		-- U ou M
		UouM 	    : out std_logic										-- 0 = ULA e 1 = Memoria
	);

end component;

component GerenciadorInstrucoes is

	generic (
		tDados		: natural := 16;
		tMnemonico	: natural :=  4;
		tMemoria	: natural := 12 --2^12 = 4096
	);

	port (
		relogio		: in  std_logic;
		reset		: in  std_logic;
		-- Controle
		gravando	: in  std_logic;
		sucessoComp	: in  std_logic;
		-- Entrada
		instrucao 	: in  std_logic_vector(tDados 	 - 1 downto 0);
		-- Saida
		endProximo	: out std_logic_vector(tMemoria	 - 1 downto 0)
	);

end component;

component Gravador is

	generic (
		tDados		: natural := 16;
		tMemoriaP	: natural := 12;
		instAGravar	: natural :=  6
	);

	port (
		relogio		: in  std_logic;
		-- Entrada para evitar simplificacao do design
		inst		: in  std_logic_vector(tDados		- 1 downto 0);
		ende		: in  std_logic_vector(tMemoriaP	- 1 downto 0);
		-- Controle externo
		ativaGrav	: in  std_logic;
		ativaGravD	: in  std_logic;
		--Saida
		gravando	: out std_logic;
		enderecoMP	: out std_logic_vector(tMemoriaP	- 1 downto 0);
		instrucao	: out std_logic_vector(tDados		- 1 downto 0)
	);
	
end component;

component Memoria is

	generic(
		tDados		: natural := 16;
		tMemoria	: natural :=  9 --2^9 = 512
	);

	port(
		relogio  	: in  std_logic;
		-- Controle
		ativaE		: in  std_logic;
		-- Entrada
		valorE		: in  std_logic_vector(tDados	- 1 downto 0);
		endereco	: in  std_logic_vector(tMemoria - 1 downto 0);
		-- Saida
		valorS		: out std_logic_vector(tDados	- 1 downto 0);
		-- Entradas e saidas especiais
		entradaE	: in  std_logic_vector(tDados	- 1 downto 0);
		saidaE		: out std_logic_vector(tDados	- 1 downto 0)
	);

end component;

component MemoriaPrograma is

generic(
	tDados			: natural := 16;
	tMemoriaP		: natural := 12 --2^12 = 4096
);

port(
	relogio  		: in  std_logic;
	-- Controle
	ativaE			: in  std_logic;
	-- Entrada
	valorE			: in  std_logic_vector(tDados		- 1 downto 0);
	enderecoE		: in  std_logic_vector(tMemoriaP	- 1 downto 0);
	enderecoL		: in  std_logic_vector(tMemoriaP	- 1 downto 0);
	-- Saida
	valorS			: out std_logic_vector(tDados		- 1 downto 0)
);

end component;

component ParticionadorInstrucoes is

	generic (
		tDados		: natural := 16;
		tMemoriaBR	: natural :=  3;		--2^3  = 8
		tMemoria	: natural :=  9; 	--2^9  = 512
		tImediato	: natural :=  9;
		tMnemonico	: natural :=  4
	);

	port (
		-- Entrada
		instrucao	: in  std_logic_vector(tDados		- 1 downto 0);
		-- Sem Controle
		--Saida
		enderecoMem	: out std_logic_vector(tMemoria	- 1 downto 0);
		imediato 	: out std_logic_vector(tDados		- 1 downto 0);
		REG1		: out std_logic_vector(tMemoriaBR	- 1 downto 0);
		REG2		: out std_logic_vector(tMemoriaBR	- 1 downto 0);
		REG3		: out std_logic_vector(tMemoriaBR	- 1 downto 0)
	);

end component;

component ULA is

	generic (
		tDados:		natural := 16;
		nbitsCodOp:	natural := 3
	);

	port (
		-- Controle
		ativaAri	: in  std_logic;
		ativaComp	: in  std_logic;
		ativaLogi	: in  std_logic;
		operacao	: in  std_logic_vector (nbitsCodOp	- 1 downto 0);
		-- Entradas
		entrada0 	: in  std_logic_vector (tDados		- 1 downto 0);
		entrada1 	: in  std_logic_vector (tDados		- 1 downto 0);
		-- Saidas
		valorSai	: out std_logic_vector (tDados		- 1 downto 0);
		valorAlt	: out std_logic_vector (tDados		- 1 downto 0);
		modifCP		: out std_logic
	);

end component;

component UouM is

	generic (
		tDados	: natural := 16
	);

	port (
		-- Controle
		UouM		: in  std_logic;
		-- Entrada
		ULA			: in  std_logic_vector(tDados	- 1 downto 0);
		Mem			: in  std_logic_vector(tDados	- 1 downto 0);
		-- Saida
		Saida		: out std_logic_vector(tDados	- 1 downto 0)
	);

end component;

signal instrucao			: std_logic_vector(tDados - 1 downto 0);
signal operacao				: std_logic_vector(nbitsCodOp - 1 downto 0);

signal ativaEBR, ativaA, ativaAri, ativaComp, ativaLogi : std_logic;
signal valorEBR, valorABR, valorS0BR, valorS1BR : std_logic_vector(tDados - 1 downto 0);
signal enderecoL0BR, enderecoL1BR, enderecoEBR	: std_logic_vector(tMemoriaBR - 1 downto 0);

signal aBouI, aUouM 		:std_logic;
signal PIpBouI, BouIpULA 	: std_logic_vector(tDados - 1 downto 0);

signal ativaEM				: std_logic;
signal saidaULA, valorSM 	: std_logic_vector(tDados - 1 downto 0);
signal enderecoM			: std_logic_vector(tMemoria - 1 downto 0);

signal valorEMP, entradaEMP, saidaEMP : std_logic_vector(tDados - 1 downto 0);
signal enderecoEMP			: std_logic_vector(tMemoriaP -1 downto 0);

signal sucessoC, gravando	: std_logic;
signal endProximo			: std_logic_vector(tMemoriaP - 1 downto 0);
signal instrucaoPGrav 		: std_logic_vector(tDados - 1 downto 0);

begin

-- Testes

-- Gravador
--grav <= gravando;
--enderecoEMPG <= enderecoEMP;
--instrucaoPG <= instrucaoPGrav;

-- Memoria de Programa
--endLMP <= endProximo;
instru <= instrucao;

-- Particionador de Intrucoes
endM <= enderecoM;
PpB <= PIpBouI;
endEBR <= enderecoEBR;
endL0BR <= enderecoL0BR;
endL1BR <= enderecoL1BR;

-- Testes

C1: BancoRegistradores
generic map (tDados, tMemoriaBR, endZeroBR)
port map (relogio, ativaEBR, ativaA, valorEBR,
		  valorABR, enderecoL0BR, enderecoL1BR, enderecoEBR,
		  valorS0BR, valorS1BR);

C2: BouI
generic map (tDados)
port map (aBouI, valorS0BR, PIpBouI, BouIpULA);

C3: Controle
generic map (tDados, tMnemonico, nbitsCodOp)
port map (reset, relogio, instrucao, operacao, ativaAri,
		  ativaComp, ativaLogi, ativaEBR, ativaA,
		  ativaEM, aBouI, aUouM);

C4: GerenciadorInstrucoes
generic map(tDados, tMnemonico, tMemoriaP)
port map (relogio, reset, gravando, sucessoC,
		  instrucao, endProximo);

C5: Gravador
generic map (tDados, tMemoriaP)
port map (relogio, inst, ende, ativaGrav, ativaGravD, gravando,
		  enderecoEMP, instrucaoPGrav);

C6: Memoria
generic map (tDados, tMemoria)
port map (relogio, ativaEM, saidaULA, enderecoM,
		  valorSM, entradaE, saidaE);

C7: MemoriaPrograma
generic map (tDados, tMemoriaP)
port map (relogio, ativaGrav, instrucaoPGrav, enderecoEMP,
		   endProximo, instrucao);

C8: ParticionadorInstrucoes
generic map (tDados, tMemoriaBR, tMemoria, tImediato, tMnemonico)
port map (instrucao, enderecoM, PIpBouI, enderecoEBR,
		  enderecoL0BR, enderecoL1BR);

C9: ULA
generic map (tDados, nbitsCodOp)
port map (ativaAri, ativaComp, ativaLogi, operacao,
		  BouIpULA, valorS1BR, saidaULA, valorABR, sucessoC);

CA: UouM
generic map (tDados)
port map (aUouM, saidaULA, valorSM, valorEBR);

end rtl;
